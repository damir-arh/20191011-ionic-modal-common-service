import { ModalPage } from './modal/modal.page';
import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  constructor(private modalCtrl: ModalController) {}

  async openModal(modalPage: typeof ModalPage) {
    const modal = await this.modalCtrl.create({
      component: modalPage
    });
    await modal.present();
  }
}
