import { NavigationService } from './../navigation.service';
import { Component } from '@angular/core';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(private navigation: NavigationService) {}

  async openModal() {
    this.navigation.openModal(ModalPage);
  }
}
